from django.shortcuts import render, reverse, redirect
from django.contrib.auth.models import User, Permission
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate, logout, login

from django.conf.urls.static import static

from .forms import TRForm, UpdateTRForm
import pandas as pd
from django_tables2.tables import Table
from django.contrib.staticfiles import finders
from django.contrib.staticfiles.storage import staticfiles_storage

from farmakami.settings import STATIC_DIR
import os
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def createTR(request):
    if request.method =='POST':
        nama_dokter = request.POST.get("nama_dokter")
        nama_pasien = request.POST.get("nama_pasien")
        isi_resep = request.POST.get("isi_resep")
        unggahan_resep = request.POST.get("unggahan_resep")
        tanggal =  request.POST["waktu"]
        no_telepon =  request.POST.get("no_telepon")
        id_transaksi = request.POST["id_transaksi_select"]
        cursor = connection.cursor()
    
        # sumid = "select count(*) from farmakami.pengantaran_farmasi"
        # cursor.execute(sumid)
        # sumidfix = cursor.fetchone()[0]
        # sql_last_id = "select id_pengantaran from farmakami.pengantaran_farmasi limit " + str(sumidfix) + " offset " + str((sumidfix-1)) + ""
        # cursor.execute(sql_last_id)
        # last_id = cursor.fetchone()
        # last_id = int(last_id[0][3:])
        # last_id = last_id + 1
        # fix_id = "PTR" + str(last_id)

        #Mencari ID
        if id_transaksi != 'none':
            sumid = "select count(*) from farmakami.transaksi_dengan_resep"
            cursor.execute(sumid)
            sumidfix = cursor.fetchone()[0]

            test = "R" + str(sumidfix)
            sql_test = "select count(*) from farmakami.transaksi_dengan_resep where no_resep ='" + test + "'"
            cursor.execute(sql_test)
            hasil = cursor.fetchone()[0]

            while(hasil != 0):
                sumidfix = sumidfix + 1
                test = "R" + str(sumidfix)
                sql_test = "select count(*) from farmakami.transaksi_dengan_resep where no_resep ='" + test + "'"
                cursor.execute(sql_test)
                hasil = cursor.fetchone()[0]

            fix_id = "R" + str(sumidfix)
            #Sampe sini
            status_validasi = 'Processing'
            sql_bp = "insert into farmakami.transaksi_dengan_resep VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, NULL);"
            cursor.execute(sql_bp,(fix_id, nama_dokter, nama_pasien, isi_resep, unggahan_resep, tanggal, status_validasi, no_telepon, id_transaksi))

            return redirect('/transaksi_resep/listTR')
    form = TRForm()
    cursor = connection.cursor()
    sql_idtransaksi = "select distinct id_transaksi_pembelian from farmakami.transaksi_pembelian ORDER BY id_transaksi_pembelian ASC"
    cursor.execute(sql_idtransaksi)
    idtransaksi = namedtuplefetchall(cursor)
    args = {'form': form, 'idtransaksi': idtransaksi}
    return render(request, 'createtr.html', args)


def listTR(request):
    dataset = []
    cursor = connection.cursor()
    sql_all_PF= "select * from farmakami.transaksi_dengan_resep"
    cursor.execute(sql_all_PF)
    dataset = namedtuplefetchall(cursor)
    context = {'table': dataset}
    return render(request, "listTR.html", context)

def deleteTR(request, id):
    cursor = connection.cursor()
    sql_delete_TR= "delete from farmakami.transaksi_dengan_resep where no_resep = '" +  id + "'"
    cursor.execute(sql_delete_TR)
    return redirect('/transaksi_resep/listTR')



def updateTR(request, id):
    if request.method == "POST":
        nama_dokter = request.POST.get("nama_dokter")
        nama_pasien = request.POST.get("nama_pasien")
        isi_resep = request.POST.get("isi_resep")
        unggahan_resep = request.POST.get("unggahan_resep")
        tanggal =  request.POST["tanggal"]
        no_telepon =  request.POST.get("no_telepon")
        id_transaksi = request.POST["id_transaksi_select"]
        status_validasi = request.POST.get("status_validasi")
        email_apoteker = request.POST["email_apoteker_select"]

        cursor = connection.cursor()
        if nama_dokter != "":
            sql = "update farmakami.transaksi_dengan_resep set nama_dokter ='" + nama_dokter +"' where no_resep ='" + id + "';"
            cursor.execute(sql)
        
        if nama_pasien != "":
            sql = "update farmakami.transaksi_dengan_resep set nama_pasien ='" + nama_pasien +"' where no_resep ='" + id + "';"
            cursor.execute(sql)
        
        if isi_resep != "":
            sql = "update farmakami.transaksi_dengan_resep set isi_resep ='" + isi_resep +"' where no_resep ='" + id + "';"
            cursor.execute(sql)
        
        if unggahan_resep != "":
            sql = "update farmakami.transaksi_dengan_resep set unggahan_resep ='" + unggahan_resep +"' where no_resep ='" + id + "';"
            cursor.execute(sql)
        
        if tanggal != "":
            sql = "update farmakami.transaksi_dengan_resep set tanggal_resep ='" + tanggal +"' where no_resep ='" + id + "';"
            cursor.execute(sql)
        
        if no_telepon != "":
            sql = "update farmakami.transaksi_dengan_resep set no_telepon ='" + no_telepon +"' where no_resep ='" + id + "';"
            cursor.execute(sql)
        
        if status_validasi != "":
            sql = "update farmakami.transaksi_dengan_resep set status_validasi ='" + status_validasi +"' where no_resep ='" + id + "';"
            cursor.execute(sql)

        if id_transaksi != "none":
            sql = "update farmakami.transaksi_dengan_resep set id_transaksi_pembelian ='" + id_transaksi +"' where no_resep ='" + id + "';"
            cursor.execute(sql)
        
        if email_apoteker != "none":
            sql = "update farmakami.transaksi_dengan_resep set email_apoteker ='" + email_apoteker +"' where no_resep ='" + id + "';"
            cursor.execute(sql)
        
        return redirect('/transaksi_resep/listTR')
    form = UpdateTRForm()
    cursor = connection.cursor()
    sql_idtransaksi = "select distinct id_transaksi_pembelian from farmakami.transaksi_pembelian ORDER BY id_transaksi_pembelian ASC"
    cursor.execute(sql_idtransaksi)
    idtransaksi = namedtuplefetchall(cursor)
    sql_emailapoteker = "select distinct email from farmakami.apoteker"
    cursor.execute(sql_emailapoteker)
    emailapoteker= namedtuplefetchall(cursor)
    args = {'form': form, 'idtransaksi': idtransaksi, 'emailapoteker': emailapoteker, "id":id}
    return render(request, "updatetr.html", args)

