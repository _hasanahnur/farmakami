from django import forms
import datetime
# from .widgets import BootstrapDateTimePickerInput

class TRForm(forms.Form):
    nama_dokter = forms.CharField(label='Nama Dokter', max_length=50)
    nama_pasien =  forms.CharField(label='Nama Pasien', max_length=50)
    isi_resep=  forms.CharField(label='Isi Resep', max_length=100)
    unggahan_resep =  forms.CharField(label='Unggahan Resep', max_length=100)
    tanggal =  forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M'])
    no_telepon =  forms.CharField(label='No Telepon', max_length=20)
    id_transaksi =  forms.CharField(label='ID Transaksi', max_length=10)

class UpdateTRForm(forms.Form):
    nama_dokter = forms.CharField(label='Nama Dokter', max_length=50, required=False)
    nama_pasien =  forms.CharField(label='Nama Pasien', max_length=50, required=False)
    isi_resep=  forms.CharField(label='Isi Resep', max_length=100, required=False)
    unggahan_resep =  forms.CharField(label='Unggahan Resep', max_length=100, required=False)
    tanggal =  forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M'], required=False)
    no_telepon =  forms.CharField(label='No Telepon', max_length=20, required=False)
    status_validasi =  forms.CharField(label='Status Validasi', max_length=20, required=False)
    id_transaksi =  forms.CharField(label='ID Transaksi', max_length=10, required=False)
    email_apoteker =  forms.CharField(label='ID Transaksi', max_length=50, required=False)