from django.urls import path, include
from .views import createTR, listTR, deleteTR, updateTR

app_name = "transaksi_resep"
urlpatterns = [
    path('createTR', createTR, name="createTR"),
    path('listTR', listTR, name="listTR"),
    path('deleteTR/<id>/', deleteTR, name="deleteTR"),
    path('updateTR/<id>/', updateTR, name="updateTR"),
]