from django.shortcuts import render, reverse, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.db import connection
from collections import namedtuple
import random


from .forms import LoginForm, RegAdmin, RegKonsumen, RegKurir, RegCS, alamatForm

# Create your views here.

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def welcome(request):
     try:
        role = request.session['role']
        return redirect('/login/homepage')
     except:
        pass
     return render(request, "welcome.html")

def alamat(request, id):
     if request.method == "POST":
          status = request.POST.get("status")
          alamat = request.POST.get("alamat")
          print(status)
          print(alamat)
          cursor = connection.cursor()
          sql = "insert into farmakami.alamat_konsumen values(%s, %s, %s)"
          cursor.execute(sql, (id, alamat, status))
          link = '/login/alamat/' + id
          return redirect(link)
     form = alamatForm()
     args = {'form': form, "id":id }
     link = '/login/alamat/' + id
     return render(request, "alamat.html", args)

def hpalamat(request, id):
     return redirect('/login/login')

def login(request):
     message = ''
     if request.method =='POST':
         email = request.POST.get("username")
         password = request.POST.get("password")

         cursor = connection.cursor()

         sql = "select email, password from farmakami.pengguna where email = " + "'" + email + "'" 
         cursor.execute(sql)
         auth = cursor.fetchone()
         
         message = 'Invalid email or password'
         if auth != None:
          if password == auth[1]:
               request.session['email'] = email
               if search_role(email, 'konsumen'):
                    request.session['role'] = 'konsumen'
               elif search_role(email, 'admin_apotek'):
                    request.session['role'] = 'admin_apotek'
               elif search_role(email, 'cs'):
                    request.session['role'] = 'cs'
               elif search_role(email, 'kurir'):
                    request.session['role'] = 'kurir'
               print(request.session['email'])
               print(request.session['role'])
               return redirect('/login/homepage')
           
     form = LoginForm()
     args = {'form': form, 'message' : message }
     return render(request, 'login.html', args)

def regchoice(request):
    return render(request, "register_choice.html")

def validate(request):
    return render(request, "homepage.html")

def register_admin(request):
     if request.method =='POST':
        email = request.POST.get("username")
        password = request.POST.get("password")
        nama_lengkap = request.POST.get("nama_lengkap")
        no_telp = request.POST.get("no_telp")
        role = "admin"

        cursor = connection.cursor()

        sql_count_pengguna = "select count(*) from farmakami.pengguna where email = " + "'" + email + "'" 
        cursor.execute(sql_count_pengguna)
        count_pengguna = cursor.fetchone()

        sql_count_apoteker = "select count(*) from farmakami.apoteker where email = " + "'" + email + "'" 
        cursor.execute(sql_count_apoteker)
        count_apoteker = cursor.fetchone()
        
        sql_apotek = "select id_apotek from farmakami.apotek"
        cursor.execute(sql_apotek)
        apotek = namedtuplefetchall(cursor)
        pilih_apotek = random.choice(apotek)

        if count_pengguna[0] == 0 and count_apoteker[0] == 0 :
             sql_pengguna = "insert into farmakami.pengguna VALUES (%s, %s, %s, %s);"
             cursor.execute(sql_pengguna,(email, no_telp, password, nama_lengkap))
             sql_apoteker = "insert into farmakami.apoteker VALUES ('" + email + "');"
             cursor.execute(sql_apoteker)
             sql_admin = "insert into farmakami.admin_apotek VALUES (%s, %s);"
             cursor.execute(sql_admin,(email, pilih_apotek[0]))

        return redirect('/login/login')
     form = RegAdmin()
     args = {'form': form}
     return render(request, 'register.html', args)

def register_konsumen(request):
     message = ""
     if request.method =='POST':
         email = request.POST.get("username")
         password = request.POST.get("password")
         nama_lengkap = request.POST.get("nama_lengkap")
         no_telp = request.POST.get("no_telp")
         jenis_kelamin = request.POST["jenis_kelamin"]
         print(jenis_kelamin)
         tanggal_lahir = request.POST.get("tanggal_lahir")
         print(tanggal_lahir)
         role = "konsumen"

         cursor = connection.cursor()

         sql_count_pengguna = "select count(*) from farmakami.pengguna where email = " + "'" + email + "'" 
         cursor.execute(sql_count_pengguna)
         count_pengguna = cursor.fetchone()

         sql_count_konsumen = "select count(*) from farmakami.konsumen where email = " + "'" + email + "'" 
         cursor.execute(sql_count_konsumen)
         count_konsumen = cursor.fetchone()


         sumid = "select count(*) from farmakami.konsumen"
         cursor.execute(sumid)
         sumidfix = cursor.fetchone()[0]
         sql_last_id = "select id_konsumen from farmakami.konsumen limit " + str(sumidfix) + " offset " + str((sumidfix-1)) + ""
         cursor.execute(sql_last_id)
         last_id = cursor.fetchone()
         last_id = int(last_id[0][1:])
         last_id = last_id + 1
         fix_id = "P" + str(last_id)
         print(fix_id)
         message = 'Invalid email or password'

         if count_pengguna[0] == 0 and count_konsumen[0] == 0 :
             sql_pengguna = "insert into farmakami.pengguna VALUES (%s, %s, %s, %s);"
             cursor.execute(sql_pengguna,(email, no_telp, password, nama_lengkap))
             sql_konsumen = "insert into farmakami.konsumen VALUES (%s, %s, %s, %s);"
             cursor.execute(sql_konsumen,(fix_id, email, jenis_kelamin, tanggal_lahir))
             link = '/login/alamat/' + fix_id
             return redirect(link)
     form = RegKonsumen()
     args = {'form': form, 'message':message}
     return render(request, 'register.html', args)

def register_kurir(request):
     message = ""
     if request.method =='POST':
          email = request.POST.get("username")
          password = request.POST.get("password")
          nama_lengkap = request.POST.get("nama_lengkap")
          no_telp = request.POST.get("no_telp")
          nama_perusahaan = request.POST.get("nama_perusahaan")
          role = "kurir"

          cursor = connection.cursor()
          sql_found_email = "select email from farmakami.PENGGUNA WHERE email = '"+ email +"' ;"
          cursor.execute(sql_found_email)
          email_pengguna = cursor.fetchone()
          if email_pengguna == None:
               sql_count_pengguna = "select count(*) from farmakami.kurir" 
               cursor.execute(sql_count_pengguna)
               sumidfix = cursor.fetchone()[0]

               sql_count_kurir = "select id_kurir from farmakami.kurir limit " + str(sumidfix) + " offset " + str((sumidfix-1)) + ""
               cursor.execute(sql_count_kurir)
               last_id = cursor.fetchone()
               last_id = int(last_id[0][1:])
               last_id = last_id + 1
               fix_id = " K" + str(last_id)



               sumid = "select count(*) from farmakami.kurir"
               cursor.execute(sumid)
               sumidfix = cursor.fetchone()[0]
               test = "K" + str(sumidfix)
               sql_test = "select count(*) from farmakami.kurir where id_kurir='"+ test +"'"
               cursor.execute(sql_test)
               hasil = cursor.fetchone()[0]
               message = 'Invalid email or password'
               while(hasil != 0):
                         sumidfix = sumidfix + 1
                         test = "K" + str(sumidfix)
                         sql_test = "select count(*) from farmakami.kurir where id_kurir ='" + test + "'"
                         cursor.execute(sql_test)
                         hasil = cursor.fetchone()[0]
               fix_id = "K" + str(sumidfix)
               sql_pengguna = "insert into farmakami.pengguna VALUES (%s, %s, %s, %s);"
               cursor.execute(sql_pengguna,(email, no_telp, password, nama_lengkap))
               sql_kurir = "insert into farmakami.kurir VALUES (%s, %s, %s);"
               cursor.execute(sql_kurir,(fix_id, email, nama_perusahaan))
               return redirect('/login/login')
          else :
               message = 'Email already exist'
     form = RegKurir()
     args = {'form': form, 'message':message}
     return render(request, 'register.html', args)


def register_cs(request):
     message = ""
     if request.method =='POST':
          email = request.POST.get("username")
          password = request.POST.get("password")
          nama_lengkap = request.POST.get("nama_lengkap")
          no_telp = request.POST.get("no_telp")
          no_ktp = request.POST.get("no_ktp")
          no_sia = request.POST.get("no_sia")
          role = "cs"

          cursor = connection.cursor()
          sql_found_cs = "select no_ktp from farmakami.cs where no_ktp='"+ no_ktp +"';"
          cursor.execute(sql_found_cs)
          print(cursor.fetchone())
          hasil_cs = cursor.fetchone()
          print(hasil_cs)
          if hasil_cs == None:
               cursor = connection.cursor()
               sql_found_email = "select email from farmakami.PENGGUNA WHERE email = '"+ email +"' ;"
               cursor.execute(sql_found_email)
               email_pengguna = cursor.fetchone()
               if email_pengguna == None:
                    cursor = connection.cursor()
                    sql_found_nosia = "select no_sia from farmakami.cs WHERE no_sia = '"+ no_sia +"' ;"
                    cursor.execute(sql_found_nosia)
                    no_sia_cs= cursor.fetchone()
                    if no_sia_cs == None:
                         sql_pengguna = "insert into farmakami.pengguna VALUES (%s, %s, %s, %s);"
                         cursor.execute(sql_pengguna,(email, no_telp, password, nama_lengkap))
                         sql_apoteker = "insert into farmakami.apoteker VALUES ('" + email +"');"
                         cursor.execute(sql_apoteker)
                         sql_cs = "insert into farmakami.cs VALUES (%s, %s, %s);"
                         cursor.execute(sql_cs,(no_ktp, email, no_sia))
                         return redirect('/login/login')
                    else :
                         message = 'No Sia already exist'
               else :
                    message = 'Email already exist'
          else :
               message = 'No KTP already exist'
     form = RegCS()
     args = {'form': form, 'message': message}
     return render(request, 'register.html', args)

def homepage(request):
    try:
        email = request.session['email']
        role = request.session['role']
        print(request.session['email'])
        print(role)
        if role == "admin_apotek" or role == None:
           return render(request, "hp_all.html")
        elif role == "cs":
           return render(request, "hp_cs.html")
        elif role == "konsumen":
           return render(request, "hp_konsumen.html")
        elif role == "kurir":
           return render(request, "hp_kurir.html")
    except:
          return render(request, "hp_all.html")

def logout(request):
     try:
          del request.session['email']
          del request.session['role']
          return redirect('/')
     except:
          pass
     return redirect('/')

def search_role(email, role):
     cursor = connection.cursor()
     sql = "select * from farmakami."+ role + " where email ='" + email + "';"
     cursor.execute(sql)
     sql_role = namedtuplefetchall(cursor)
     if len(sql_role) == 0:
          return False
     else:
          return True

def profile(request):
    email = request.session['email']
    role = request.session['role']
    cursor = connection.cursor()

    sql = "select * from farmakami.pengguna where email ='" + email + "'"
    cursor.execute(sql)
    dataset = namedtuplefetchall(cursor)
    baseProfile = dataset[0]

    if role == "admin_apotek" or role == None:
      context = {'item': dataset[0]}
      return render(request, "profile.html", context)

    elif role == "konsumen":
      sql = "select * from farmakami.konsumen where email ='" + email + "'"
      cursor.execute(sql)
      dataset = namedtuplefetchall(cursor)
      conProfile = dataset[0]

      id_konsumen = conProfile.id_konsumen
      sql = "select * from farmakami.alamat_konsumen where id_konsumen ='" + id_konsumen + "'"
      cursor.execute(sql)
      dataset = namedtuplefetchall(cursor)
      address = dataset

      other = {'item2': conProfile, 'item1':baseProfile, 'item3': address}
      return render(request, "profile_konsumen.html", other)

    elif role == "kurir":
      sql = "select * from farmakami.kurir where email ='" + email + "'"
      cursor.execute(sql)
      dataset = namedtuplefetchall(cursor)
      perusahaan = dataset[0]

      context = {'item1': baseProfile, 'item2':perusahaan}
      return render(request, "profile_kurir.html", context)

    elif role == "cs":
      sql = "select * from farmakami.cs where email ='" + email + "'"
      cursor.execute(sql)
      dataset = namedtuplefetchall(cursor)
      data = dataset[0]

      context = {'item1': baseProfile, 'item2':data}
      return render(request, "profile_cs.html", context)

