from django import forms
import datetime

class LoginForm(forms.Form):
    username = forms.CharField(label='Username:', max_length=50)
    password = forms.CharField(label='Password:', max_length=128)

class RegAdmin(forms.Form):
    username = forms.CharField(label='Username', max_length=50)
    password = forms.CharField(label='Password', max_length=128)
    nama_lengkap = forms.CharField(label='Nama Lengkap', max_length=50)
    no_telp = forms.CharField(label='No Telepon', max_length=20)

class RegKonsumen(forms.Form):
    username = forms.CharField(label='Username', max_length=50)
    password = forms.CharField(label='Password', max_length=128)
    nama_lengkap = forms.CharField(label='Nama Lengkap', max_length=50)
    no_telp = forms.CharField(label='No Telepon', max_length=20)
    jk_choices = (("P", "P"), ("L", "L"))
    jenis_kelamin = forms.ChoiceField(choices = jk_choices) 
    # jenis_kelamin = forms.CharField(label='Jenis Kelamin', max_length=1)
    # tanggal_lahir = forms.DateField(label='Tanggal Lahir', widget=forms.SelectDateWidget(empty_label="Nothing"))
    tanggal_lahir = forms.DateField(label='Tanggal Lahir (yyyy-mm-dd)', initial=datetime.date.today)

class alamatForm(forms.Form):
    status = forms.CharField(label='Status Alamat', max_length=20, required = False)
    alamat = forms.CharField(label='Alamat', max_length=100, required = False)

class RegKurir(forms.Form):
    username = forms.CharField(label='Username', max_length=50)
    password = forms.CharField(label='Password', max_length=128)
    nama_lengkap = forms.CharField(label='Nama Lengkap', max_length=50)
    no_telp = forms.CharField(label='No Telepon', max_length=20)
    nama_perusahaan = forms.CharField(label='Nama Perusahaan', max_length=50)

class RegCS(forms.Form):
    username = forms.CharField(label='Username', max_length=50)
    password = forms.CharField(label='Password', max_length=128)
    nama_lengkap = forms.CharField(label='Nama Lengkap', max_length=50)
    no_telp = forms.CharField(label='No Telepon', max_length=20)
    no_sia = forms.CharField(label='No SIA', max_length=20)
    no_ktp = forms.CharField(label='No KTP', max_length=20)



