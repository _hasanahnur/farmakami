from django.urls import path, include
from .views import login, register_admin, register_konsumen, register_kurir, register_cs, validate, regchoice, homepage, alamat, hpalamat, logout, profile

app_name = "login"
urlpatterns = [
    path('login', login, name="login"),
    path('regadmin', register_admin, name="regadmin"),
    path('regkonsumen', register_konsumen, name="regkonsumen"),
    path('regkurir', register_kurir, name="regkurir"),
    path('regcs', register_cs, name="regcs"),
    path('regchoice', regchoice, name="regchoice"),
    path('homepage', homepage, name="homepage"),
    path('alamat/<id>/', alamat, name='alamat'),
    path('hpalamat/<id>/', hpalamat, name='hpalamat'),
    path('logout', logout, name='logout'),
    path('profile', profile, name='profile')
    # path('submitalamat/<id>/', submitalamat, name='submitalamat')
]
