from django.shortcuts import render, reverse, redirect
from django.contrib.auth.models import User, Permission
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate, logout, login

from django.conf.urls.static import static

from .forms import ListPDForm, updateListPDForm, ListPDForm2
import pandas as pd
from django_tables2.tables import Table
from django.contrib.staticfiles import finders
from django.contrib.staticfiles.storage import staticfiles_storage

from farmakami.settings import STATIC_DIR
import os
from django.db import connection
from collections import namedtuple


def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def createlistPD(request):
    message = ''
    cursor = connection.cursor()
    if request.method =='POST':
        form = ListPDForm(request.POST)
        id_produk = request.POST["id_produk_select"]
        if id_produk != "none":
            sql_cek_apotek = "select count(*) from farmakami.produk_apotek where id_produk ='" + id_produk +"' and stok > 0;"
            cursor.execute(sql_cek_apotek)
            jumlah_apotek = cursor.fetchone()[0]
            if jumlah_apotek != 0 :
                return redirect('/list_produk_dibeli/createListPD2/'+ id_produk +'/')
            else :
                message = "Stok Produk tidak tersedia di Apotek manapun"
        else:
            message = "Mohon dilengkapi"

    form = ListPDForm()
    sql_produk = "select distinct id_produk from farmakami.produk_apotek ORDER BY id_produk ASC"
    cursor.execute(sql_produk)
    idproduk = namedtuplefetchall(cursor)
    args = {'form': form, 'idproduk': idproduk, 'message':message}
    return render(request, 'createlistpd.html', args)

def createlistPD2(request, id_produk):
    message = ''
    
    if request.method =='POST':
        id_apotek = request.POST["id_apotek_select"]
        id_transaksi_pembelian = request.POST["id_transaksi_select"]
        jumlah = request.POST.get("jumlah")
        cursor = connection.cursor()
        print(id_apotek)
        print(id_transaksi_pembelian)

        if id_apotek != 'none' and id_transaksi_pembelian != 'none': 
            sql_search_PK = "select count(*) from farmakami.list_produk_dibeli where id_produk = '"+ id_produk +"' and id_apotek = '"+ id_apotek +"' and id_transaksi_pembelian = '"+ id_transaksi_pembelian +"';"
            cursor.execute(sql_search_PK)
            # print(cursor.fetchone()[0])
            jumlah_pk = cursor.fetchone()[0]
            if jumlah_pk == 0:
                if int(jumlah) > 0 :
                    sql_pd = "insert into farmakami.list_produk_dibeli VALUES (%s, %s, %s, %s);"
                    cursor.execute(sql_pd,(jumlah, id_apotek, id_produk, id_transaksi_pembelian))
                    return redirect('/list_produk_dibeli/listPD')
                else:
                    message = "Jumlah kurang dari 0"
            else :
                message = "List Produk sudah ada"
        else :
            message = "Mohon dilengkapi"
    form = ListPDForm2()
    cursor = connection.cursor()
    sql_apotek = "select id_apotek from farmakami.produk_apotek where id_produk ='" + id_produk +"' and stok > 0 ORDER BY id_apotek ASC;"
    cursor.execute(sql_apotek)
    idapotek = namedtuplefetchall(cursor)
    sql_idtransaksi = "select distinct id_transaksi_pembelian from farmakami.transaksi_pembelian ORDER BY id_transaksi_pembelian ASC"
    cursor.execute(sql_idtransaksi)
    idtransaksi = namedtuplefetchall(cursor)
    args = {'form': form, 'idapotek': idapotek, 'idtransaksi': idtransaksi, 'idproduk':id_produk, 'message':message}
    return render(request, 'createlistpd2.html', args)

def listPD(request):
    dataset = []
    cursor = connection.cursor()
    sql_all_PD= "select * from farmakami.LIST_PRODUK_DIBELI ORDER BY id_apotek ASC"
    cursor.execute(sql_all_PD)
    dataset = namedtuplefetchall(cursor)
    context = {'table': dataset}
    return render(request, "listPD.html", context)

def updatelistPD(request, id_produk, id_apotek, id_transaksi_pembelian):
    message = ''
    if request.method == "POST":
        id_transaksi_form = request.POST["id_transaksi_select"]
        jumlah = request.POST.get("jumlah")

        cursor = connection.cursor()

    
        if str(jumlah) == '' or int(jumlah) > 0 :
            sql_search_PK = "select count(*) from farmakami.list_produk_dibeli where id_produk = '"+ id_produk +"' and id_apotek = '"+ id_apotek +"' and id_transaksi_pembelian = '"+ id_transaksi_form +"';"
            cursor.execute(sql_search_PK)
            # print(cursor.fetchone()[0])
            jumlah_pk = cursor.fetchone()[0]
            if id_transaksi_form == 'none' or jumlah_pk == 0:
                if id_transaksi_form != "none":
                    print('test')
                    sql = "update farmakami.list_produk_dibeli set id_transaksi_pembelian ='" + id_transaksi_form +"' where id_produk='" + id_produk + "' AND id_apotek='" + id_apotek + "' AND id_transaksi_pembelian ='"+ id_transaksi_pembelian +"';"
                    cursor.execute(sql)
                
                if jumlah != "":
                    sql = "update farmakami.list_produk_dibeli set jumlah ='" + jumlah +"' where id_produk='" + id_produk + "' AND id_apotek='" + id_apotek + "' AND id_transaksi_pembelian ='"+ id_transaksi_pembelian +"';"
                    cursor.execute(sql)

                return redirect('/list_produk_dibeli/listPD')
            else:
                message = "List Produk sudah ada"
        else:
            message= 'Jumlah kurang dari atau sama dengan 0'
    form = updateListPDForm()
    cursor = connection.cursor()
    sql_idtransaksi = "select distinct id_transaksi_pembelian from farmakami.transaksi_pembelian ORDER BY id_transaksi_pembelian ASC"
    cursor.execute(sql_idtransaksi)
    idtransaksi = namedtuplefetchall(cursor)
    args = {'form': form, "id_produk":id_produk, "id_apotek": id_apotek, "idtransaksi": idtransaksi, 'message':message}
    return render(request, 'updatelistpd.html', args)
    
def deletelistPD(request, id_produk, id_transaksi_pembelian, id_apotek):
    cursor = connection.cursor()
    print(id_produk)
    print(id_apotek)
    print(id_transaksi_pembelian)
    sql_delete_PF= "delete from farmakami.LIST_PRODUK_DIBELI where id_produk = '" +  id_produk + "' AND id_apotek = '" +  id_apotek + "' AND id_transaksi_pembelian = '" +  id_transaksi_pembelian + "'"
    cursor.execute(sql_delete_PF)
    return redirect('/list_produk_dibeli/listPD')



