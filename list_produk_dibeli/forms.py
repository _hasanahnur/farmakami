from django import forms


class ListPDForm(forms.Form):
    id_produk = forms.CharField(label='ID Produk', max_length=10)

class ListPDForm2(forms.Form):
    id_apotek = forms.CharField(label='ID Apotek', max_length=10)
    id_transaksi_pembelian = forms.CharField(label='ID Transaksi',max_length=10)
    jumlah = forms.IntegerField(label='Jumlah')

class updateListPDForm(forms.Form):
    id_transaksi_pembelian = forms.CharField(label='ID Transaksi', max_length=10, required=False)
    jumlah = forms.IntegerField(label='Jumlah', required=False)
