from django.urls import path, include
from .views import createlistPD, listPD, updatelistPD, deletelistPD, createlistPD2

app_name = "list_produk_dibeli"
urlpatterns = [
    path('createListPD', createlistPD, name="createListPD"),
    path('createListPD2/<id_produk>/', createlistPD2, name="createlistPD2"),
    path('listPD', listPD, name="listPD"),
    path('updatelistPD/<id_produk>/<id_apotek>/<id_transaksi_pembelian>', updatelistPD, name="updatelistPD"),
    path('deletelistPD/<id_produk>/<id_apotek>/<id_transaksi_pembelian>', deletelistPD, name="deletelistPD")
]