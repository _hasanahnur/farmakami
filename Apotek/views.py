from django.shortcuts import render, reverse, redirect
from django.contrib.auth.models import User, Permission
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate, logout, login

from django.conf.urls.static import static

from .forms import APForm,updateAPForm
import pandas as pd
from django_tables2.tables import Table
from django.contrib.staticfiles import finders
from django.contrib.staticfiles.storage import staticfiles_storage

from farmakami.settings import STATIC_DIR
import os
from collections import namedtuple
from django.db import connection

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]



def createAp(request):
    #  if request.method =='POST':
    #      return redirect('/Apotek/listAp')
    #  form = APForm()
    #  args = {'form': form}
    #  return render(request, 'createapotek.html', args)
    if request.method =='POST':
        nama_apotek = request.POST.get("nama_apotek")
        alamat_apotek = request.POST.get("alamat_apotek")
        no_telepon = request.POST.get("no_telepon")
        nama_penyelenggara = request.POST.get("nama_penyelenggara")
        no_sia_penyelenggara = request.POST.get("no_sia_penyelenggara")
        email_penyelenggara = request.POST["email_penyelenggara"]
        # id_apotek_select = request.POST["id_apotek_select"]

        cursor = connection.cursor()

        #Mencari dan membuat ID Transaksi yang baru
        sumid = "select count(*) from farmakami.apotek"
        cursor.execute(sumid)
        sumidfix = cursor.fetchone()[0]
        id_apotek = "A" + str (sumidfix + 1)


        #Sampe sini
        
        sql_bp = "insert into farmakami.apotek VALUES (%s, %s, %s, %s, %s , %s , %s);"
        cursor.execute(sql_bp,(id_apotek, email_penyelenggara, no_sia_penyelenggara, nama_penyelenggara, nama_apotek , alamat_apotek , no_telepon))
        return redirect('/Apotek/listAp')

    cursor = connection.cursor()
    sql_user = "select distinct id_apotek from farmakami.apotek "
    cursor.execute(sql_user)
    id_apotek = namedtuplefetchall(cursor)

    form = APForm()
    args = {'form': form ,"id_apotek" : id_apotek }
    return render(request, 'createapotek.html', args)




def listAp(request):
    dataset = []
    cursor = connection.cursor()
    sql_all_bp= "select * from farmakami.apotek"
    cursor.execute(sql_all_bp)
    dataset = namedtuplefetchall(cursor)
    context = {'table': dataset}
    return render(request, "listAp.html", context)


def updateAp(request , id):

    cursor = connection.cursor()
    if request.method =='POST':
        nama_apotek = request.POST.get("nama_apotek")
        alamat_apotek = request.POST.get("alamat_apotek")

        no_telepon = request.POST.get("no_telepon")
        nama_penyelenggara = request.POST.get("nama_penyelenggara")
        no_sia_penyelenggara = request.POST.get("no_sia_penyelenggara")
        email_penyelenggara = request.POST["email_penyelenggara"]
        

        if nama_apotek != "":
            sql = "update farmakami.apotek set nama_apotek ='" + nama_apotek +"' where id_apotek ='" + id + "';"
            cursor.execute(sql)
        
        if alamat_apotek != "":
            sql = "update farmakami.apotek set alamat_apotek ='" + alamat_apotek +"' where id_apotek ='" + id + "';"
            cursor.execute(sql)
        
        if no_telepon != "":
            sql = "update farmakami.apotek set telepon_apotek ='" + no_telepon +"' where id_apotek ='" + id + "';"
            cursor.execute(sql)
        
        if nama_penyelenggara != "":
            sql = "update farmakami.apotek set nama_penyelenggara ='" + nama_penyelenggara +"' where id_apotek ='" + id + "';"
            cursor.execute(sql)
        
        if no_sia_penyelenggara != "":
            sql = "update farmakami.apotek set no_sia ='" + no_sia_penyelenggara +"' where id_apotek ='" + id + "';"
            cursor.execute(sql)
        
        if email_penyelenggara != "":
            sql = "update farmakami.apotek set email ='" + email_penyelenggara +"' where id_apotek ='" + id + "';"
            cursor.execute(sql)
            
        

        return redirect('/Apotek/listAp')
    form = updateAPForm()
    args = {'form': form , "id" : id}
    return render(request, 'updateapotek.html', args)
    # cursor = connection.cursor()
    # if request.method == "POST":
    #     nama_apotek = request.POST.get("nama_apotek")
    #     alamat_apotek = request.POST.get("alamat_apotek")

    #     no_telepon = request.POST.get("no_telepon")
    #     nama_penyelenggara = request.POST.get("nama_penyelenggara")
    #     no_sia_penyelenggara = request.POST.get("no_sia_penyelenggara")
    #     email_penyelenggara = request.POST["email_penyelenggara"]
    #     id_apotek_select = request.POST["id_apotek_select"]

def deleteAp(request, id):
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("delete from apotek where id_apotek = '" + id + "'")
    return redirect('Apotek:listAp')


