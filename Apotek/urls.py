from django.urls import path, include
from .views import createAp, listAp, updateAp, deleteAp

app_name = "Apotek"
urlpatterns = [
    path('createAp', createAp, name="createAp"),
    path('listAp', listAp, name="listAp"),
    path('deleteAp/<id>/', deleteAp, name="deleteAp"),
    path('updateAp/<id>/', updateAp, name="updateAp")
]