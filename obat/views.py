from django.shortcuts import render, reverse, redirect
from django.contrib.auth.models import User, Permission
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate, logout, login

from django.conf.urls.static import static

from .forms import ObatForm, ObatFormEdit
import pandas as pd
from django_tables2.tables import Table
from django.contrib.staticfiles import finders
from django.contrib.staticfiles.storage import staticfiles_storage
from collections import namedtuple
from farmakami.settings import STATIC_DIR
import os
from django.db import connection

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def deleteobat(request, id):
    try:
        role = request.session['role']
    except:
        return redirect("/")
    
    if role not in  ('admin_apotek','cs'):
        return redirect("/")
    cursor = connection.cursor()
    sql= "select id_produk from farmakami.obat where id_obat = '" +  id + "'"
    cursor.execute(sql)
    id_produk = cursor.fetchone()[0]
    sql= "delete from farmakami.obat where id_obat = '" +  id + "'"
    cursor.execute(sql)
    sql= "delete from farmakami.produk where id_produk = '" +  id_produk + "'"
    cursor.execute(sql)
    cursor.close()
    return redirect('/obat/listobat')

def createobat(request):
     try:
        role = request.session['role']
     except:
        return redirect("/")
    
     if role not in  ('admin_apotek','cs'):
        return redirect("/")

     cursor = connection.cursor()
     if request.method =='POST':
        netto = request.POST.get("netto")
        dosis = request.POST.get("dosis")
        aturan_pakai = request.POST.get("aturan_pakai")
        bentuk_kesediaan = request.POST.get("bentuk_kesediaan")
        kontraindikasi = request.POST.get("kontraindikasi")
        id_merk_obat = request.POST["idmerkobat"]

        #Mencari ID
        sumid = "select count(*) from farmakami.produk"
        cursor.execute(sumid)
        sumidfix = cursor.fetchone()[0]

        test = "PO" + str(sumidfix)
        sql_test = "select count(*) from farmakami.produk where id_produk ='" + test + "'"
        cursor.execute(sql_test)
        hasil = cursor.fetchone()[0]

        while(hasil != 0):
            sumidfix = sumidfix + 1
            test = "PO" + str(sumidfix)
            sql_test = "select count(*) from farmakami.produk where id_produk ='" + test + "'"
            cursor.execute(sql_test)
            hasil = cursor.fetchone()[0]

        fix_id_produk = "PO" + str(sumidfix)
        #Sampe sini

        #Mencari ID
        sumid = "select count(*) from farmakami.obat"
        cursor.execute(sumid)
        sumidfix = cursor.fetchone()[0]

        test = "OB" + str(sumidfix)
        sql_test = "select count(*) from farmakami.obat where id_obat ='" + test + "'"
        cursor.execute(sql_test)
        hasil = cursor.fetchone()[0]

        while(hasil != 0):
            sumidfix = sumidfix + 1
            test = "OB" + str(sumidfix)
            sql_test = "select count(*) from farmakami.obat where id_obat ='" + test + "'"
            cursor.execute(sql_test)
            hasil = cursor.fetchone()[0]

        fix_id_obat = "OB" + str(sumidfix)
        #Sampe sini

        sql= "insert into farmakami.produk VALUES ('" + fix_id_produk + "')"
        cursor.execute(sql)

        sql= "insert into farmakami.obat VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
        cursor.execute(sql, (fix_id_obat, fix_id_produk, id_merk_obat, netto, dosis, aturan_pakai, kontraindikasi, bentuk_kesediaan))
        return redirect('/obat/listobat')

     sql_merk = "select id_merk_obat from farmakami.merk_obat"
     cursor.execute(sql_merk)
     merk = namedtuplefetchall(cursor)
     form = ObatForm()
     args = {'form': form, 'merk' : merk}
     cursor.close()
     return render(request, 'createobat.html', args)

def listobat(request):
    dataset = []
    cursor = connection.cursor()
    sql= "select * from farmakami.obat"
    cursor.execute(sql)
    dataset = namedtuplefetchall(cursor)
    context = {'table': dataset}
    cursor.close()
    return render(request, "listobat.html", context)

def updateobat(request, id):
    try:
        role = request.session['role']
    except:
        return redirect("/")
    
    if role not in  ('admin_apotek','cs'):
        return redirect("/")

    cursor = connection.cursor()
    if request.method == "POST":
        netto = request.POST.get("netto")
        dosis = request.POST.get("dosis")
        
        aturan_pakai = request.POST.get("aturan_pakai")
        bentuk_kesediaan = request.POST.get("bentuk_kesediaan")
        kontraindikasi = request.POST.get("kontraindikasi")
        id_merk_obat = request.POST["idmerkobat"]

        if netto != "":
            sql = "update farmakami.obat set netto ='" + netto +"' where id_obat ='" + id + "';"
            cursor.execute(sql)
        
        if dosis != "":
            sql = "update farmakami.obat set dosis ='" + dosis +"' where id_obat ='" + id + "';"
            cursor.execute(sql)
        
        if aturan_pakai != "":
            sql = "update farmakami.obat set aturan_pakai ='" + aturan_pakai +"' where id_obat ='" + id + "';"
            cursor.execute(sql)
        
        if bentuk_kesediaan != "":
            sql = "update farmakami.obat set bentuk_kesediaan ='" + bentuk_kesediaan +"' where id_obat ='" + id + "';"
            cursor.execute(sql)
        
        if kontraindikasi != "":
            sql = "update farmakami.obat set kontraindikasi ='" + kontraindikasi +"' where id_obat ='" + id + "';"
            cursor.execute(sql)
        
        if id_merk_obat != "":
            sql = "update farmakami.obat set id_merk_obat ='" + id_merk_obat +"' where id_obat ='" + id + "';"
            cursor.execute(sql)
        
        return redirect('/obat/listobat')
    
    cursor.execute("select id_produk from farmakami.obat where id_obat='" + id + "'")
    id_produk = cursor.fetchone()[0]
    sql_merk = "select id_merk_obat from farmakami.merk_obat"
    cursor.execute(sql_merk)
    merk = namedtuplefetchall(cursor)
    form = ObatFormEdit()
    args = {'form': form, 'merk' : merk, "id_obat" : id, "id_produk": id_produk }
    cursor.close()
    return render(request, 'updateobat.html', args)