from django.urls import path, include
from .views import createobat, listobat, deleteobat, updateobat

app_name = "obat"
urlpatterns = [
    path('createobat', createobat, name="createobat"),
    path('listobat', listobat, name="listobat"),
    path('deleteobat/<id>/', deleteobat, name="deleteobat"),
    path('updateobat/<id>/', updateobat, name="updateobat")
]