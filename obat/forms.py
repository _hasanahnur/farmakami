from django import forms

class ObatForm(forms.Form):
    netto = forms.CharField(label='Netto', max_length=10)
    dosis = forms.CharField(label='Dosis', max_length=100)
    aturan_pakai = forms.CharField(label='Aturan Pakai', max_length=100, required=False)
    kontraindikasi = forms.CharField(label='Kontraindikasi', max_length=100, required=False)
    bentuk_kesediaan = forms.CharField(label='Bentuk Kesediaan', max_length=100)

class ObatFormEdit(forms.Form):
    netto = forms.CharField(label='Netto', max_length=10, required=False)
    dosis = forms.CharField(label='Dosis', max_length=100, required=False)
    aturan_pakai = forms.CharField(label='Aturan Pakai', max_length=100, required=False)
    kontraindikasi = forms.CharField(label='Kontraindikasi', max_length=100, required=False)
    bentuk_kesediaan = forms.CharField(label='Bentuk Kesediaan', max_length=100, required=False)