from django.shortcuts import render, reverse, redirect
from django.contrib.auth.models import User, Permission
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate, logout, login

from django.conf.urls.static import static

from .forms import pkForm,updatePAForm
import pandas as pd
from django_tables2.tables import Table
from django.contrib.staticfiles import finders
from django.contrib.staticfiles.storage import staticfiles_storage

from farmakami.settings import STATIC_DIR
import os
from collections import namedtuple
from django.db import connection


def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def createprodukapotek(request):
    cursor = connection.cursor()

    if request.method =='POST':
        hargajual = request.POST.get("hargajual")
        satuanpenjualan = request.POST.get("satuanpenjualan")
        stok = request.POST.get("stok")
        id_apotek_select = request.POST["id_apotek_select"]
        id_produk_select = request.POST["id_produk_select"]
        
        sql_pk = "insert into farmakami.produk_apotek VALUES (%s, %s, %s, %s, %s );"
        cursor.execute(sql_pk,(hargajual, stok, satuanpenjualan, id_produk_select, id_apotek_select ))

        return redirect('/produkapotek/listprodukapotek')

    sql_user = "select distinct id_produk from farmakami.produk_apotek ORDER BY id_produk ASC"
    cursor.execute(sql_user)
    id_produk = namedtuplefetchall(cursor)

    sql_user = "select distinct id_apotek from farmakami.produk_apotek ORDER BY id_apotek ASC"
    cursor.execute(sql_user)
    id_apotek = namedtuplefetchall(cursor)


    form = pkForm()
    args = {'form': form , "id_produk":id_produk , "id_apotek": id_apotek }
    return render(request, 'createpk.html', args)

def listprodukapotek(request):
    dataset = []
    cursor = connection.cursor()
    sql_all_bp= "select * from farmakami.produk_apotek"
    cursor.execute(sql_all_bp)
    dataset = namedtuplefetchall(cursor)
    context = {'table': dataset}
    return render(request, "listpk.html", context)



def updatePa(request, id_produk , id_apotek ):
    
    cursor = connection.cursor()
    if request.method =='POST':
        
        # id_kurir = request.POST["id_kurir_select"]
        # id_transaksi_pembelian = request.POST["id_transaksi_select"]
        # biaya_kirim = form['biaya_kirim'].value()
        # waktu = request.POST["waktu"]
        # status_pengantaran = request.POST.get("status_pengantaran")
        hargajual = request.POST.get("harga_jual")
        satuanpenjualan = request.POST.get("satuan_penjualan")
        stok = request.POST.get("stok")
        id_apotek_select = request.POST["update_apotek"]
        id_produk_select = request.POST["update_produk"]
        
        
    
        if hargajual != "":
            sql = "update farmakami.produk_apotek set harga_jual ='" + hargajual +"' where id_produk = '" + id_produk + "' and id_apotek = '" + id_apotek + "'"
            cursor.execute(sql)

        if satuanpenjualan != "none":
            sql = "update farmakami.produk_apotek set satuan_penjualan ='" + satuanpenjualan +"' where id_produk = '" + id_produk + "' and id_apotek = '" + id_apotek + "'"
            cursor.execute(sql)
        
        if stok != "none":
            sql = "update farmakami.produk_apotek set stok ='" + stok +"' where id_produk = '" + id_produk + "' and id_apotek = '" + id_apotek + "'"
            cursor.execute(sql)
        cursor.execute("update farmakami.produk_apotek set id_apotek ='" + id_apotek_select +"' where id_produk = '" + id_produk + "' and id_apotek = '" + id_apotek + "'")
        cursor.execute("update farmakami.produk_apotek set id_produk ='" + id_produk_select +"' where id_produk = '" + id_produk + "' and id_apotek = '" + id_apotek + "'")
        
        return redirect('produkapotek:listprodukapotek')

    # form = updatePAForm()
    # args = {'form': form}
    cursor.execute("set search_path to farmakami")
    cursor.execute("select * from produk_apotek where id_produk = '" + id_produk + "' and id_apotek = '" + id_apotek + "'")
    update_pa = namedtuplefetchall(cursor)[0]
    cursor.execute("select distinct id_apotek from produk_apotek")
    # print(namedtuplefetchall(cursor))
    id_apotek = namedtuplefetchall(cursor)
    cursor.execute("select distinct id_produk from produk_apotek")
    id_produk = namedtuplefetchall(cursor)
    
    
    
    
    context = {'all': update_pa,'id_produk':id_produk, 'id_apotek':id_apotek}
    return render(request, 'updateprodukapotek.html', {'update_pa' : context})


def deletePa(request, id_produk , id_apotek):
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("delete from produk_apotek where id_produk = '" + id_produk + "' and id_apotek = '" + id_apotek + "'")
    return redirect('produkapotek:listprodukapotek')
