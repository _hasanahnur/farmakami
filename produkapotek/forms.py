from django import forms


class pkForm(forms.Form):
    hargajual = forms.CharField(label='Harga Jual', max_length=100)
    satuanpenjualan = forms.CharField(label='Satuan Penjualan', max_length=100)
    stok = forms.IntegerField(label='Stok')


class updatePAForm(forms.Form):

    hargajual = forms.CharField(label='Harga Jual', max_length=100)
    satuanpenjualan = forms.CharField(label='Satuan Penjualan', max_length=100)
    stok = forms.IntegerField(label='Stok')