from django.urls import path, include
from .views import createprodukapotek, listprodukapotek, updatePa, deletePa

app_name = "produkapotek"
urlpatterns = [
    path('createprodukapotek', createprodukapotek, name="createprodukapotek"),
    path('listprodukapotek', listprodukapotek, name="listprodukapotek"),
    path('deletePa/<id_produk>_<id_apotek>/', deletePa, name="deletePa"),
    path('updatePa/<id_produk>_<id_apotek>/', updatePa, name="updatePa")
    
]