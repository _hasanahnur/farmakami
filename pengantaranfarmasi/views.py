from django.shortcuts import render, reverse, redirect
from django.contrib.auth.models import User, Permission
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate, logout, login

from django.conf.urls.static import static

from .forms import PFForm, updatePFForm
import pandas as pd
from django_tables2.tables import Table
from django.contrib.staticfiles import finders
from django.contrib.staticfiles.storage import staticfiles_storage

from farmakami.settings import STATIC_DIR
import os
from django.db import connection
from collections import namedtuple


def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def createPF(request):
    message = ''
    if request.method =='POST':
        id_transaksi_pembelian = request.POST["id_transaksi_select"]
        biaya_kirim = request.POST.get("biaya_kirim")
        waktu = request.POST["waktu"]
        cursor = connection.cursor()
        if int(biaya_kirim)> 0:
            sumid = "select count(*) from farmakami.pengantaran_farmasi"
            cursor.execute(sumid)
            sumidfix = cursor.fetchone()[0]

            test = "PTR" + str(sumidfix)
            sql_test = "select count(*) from farmakami.pengantaran_farmasi where id_pengantaran ='" + test + "'"
            cursor.execute(sql_test)
            hasil = cursor.fetchone()[0]

            while(hasil != 0):
                sumidfix = sumidfix + 1
                test = "PTR" + str(sumidfix)
                sql_test = "select count(*) from farmakami.pengantaran_farmasi where id_pengantaran ='" + test + "'"
                cursor.execute(sql_test)
                hasil = cursor.fetchone()[0]

            fix_id = "PTR" + str(sumidfix)
            #Sampe sini
            temp_kurir = 'K1'
            temp_status = 'Processing'
            sql_bp = "insert into farmakami.pengantaran_farmasi VALUES (%s, %s, %s, %s, %s, %s, %s);"
            cursor.execute(sql_bp,(fix_id, temp_kurir, id_transaksi_pembelian, waktu, temp_status, biaya_kirim, 0))

            return redirect('/pengantaranfarmasi/listPF')
        else :
            message = "Biaya kurang dari 0"

    form = PFForm()
    cursor = connection.cursor()
    sql_idtransaksi = "select distinct id_transaksi_pembelian from farmakami.transaksi_pembelian ORDER BY id_transaksi_pembelian ASC"
    cursor.execute(sql_idtransaksi)
    idtransaksi = namedtuplefetchall(cursor)
    args = {'form': form, 'idtransaksi': idtransaksi, 'message':message}
    return render(request, 'createpf.html', args)

def listPF(request):
    email = request.session['email']
    dataset = []
    cursor = connection.cursor()
    if request.session['role'] == 'kurir':
        sql_search_kurir= "select id_kurir from farmakami.kurir where email = '" + email + "';"
        cursor.execute(sql_search_kurir)
        id_kurir = cursor.fetchone()[0]
        print(id_kurir)
        sql_all_PF= "select * from farmakami.pengantaran_farmasi where id_kurir = '" + id_kurir + "' "
        cursor.execute(sql_all_PF)
        dataset = namedtuplefetchall(cursor)
    else:
        print('masuk')
        sql_all_PF= "select * from farmakami.pengantaran_farmasi ORDER BY id_pengantaran ASC"
        cursor.execute(sql_all_PF)
        dataset = namedtuplefetchall(cursor)
    context = {'table': dataset}
    return render(request, "listPF.html", context)

def updatePF(request, id):
    message = ''
    if request.method == "POST":
        id_kurir = request.POST["id_kurir_select"]
        id_transaksi_pembelian = request.POST["id_transaksi_select"]
        biaya_kirim = request.POST.get("biaya_kirim")
        waktu = request.POST["waktu"]
        status_pengantaran = request.POST.get("status_pengantaran")
    
        cursor = connection.cursor()
        print(waktu)
        if str(biaya_kirim) == '' or int(biaya_kirim)>= 0:
            if waktu != "":
                sql = "update farmakami.pengantaran_farmasi set waktu ='" + waktu +"' where id_pengantaran ='" + id + "';"
                cursor.execute(sql)

            if id_kurir != "none":
                sql = "update farmakami.pengantaran_farmasi set id_kurir ='" + id_kurir +"' where id_pengantaran ='" + id + "';"
                cursor.execute(sql)
            
            if id_transaksi_pembelian != "none":
                sql = "update farmakami.pengantaran_farmasi set id_transaksi_pembelian ='" + id_transaksi_pembelian +"' where id_pengantaran ='" + id + "';"
                cursor.execute(sql)
            
            if biaya_kirim != "":
                sql = "update farmakami.pengantaran_farmasi set biaya_kirim ='" + biaya_kirim +"' where id_pengantaran ='" + id + "';"
                cursor.execute(sql)
            
            if status_pengantaran != "":
                sql = "update farmakami.pengantaran_farmasi set status_pengantaran ='" + status_pengantaran +"' where id_pengantaran ='" + id + "';"
                cursor.execute(sql)
            return redirect('/pengantaranfarmasi/listPF')
        else:
            message = 'Biaya kurang dari 0'
        
        
    form = updatePFForm()
    cursor = connection.cursor()
    sql_idtransaksi = "select distinct id_transaksi_pembelian from farmakami.transaksi_pembelian ORDER BY id_transaksi_pembelian ASC"
    cursor.execute(sql_idtransaksi)
    idtransaksi = namedtuplefetchall(cursor)
    sql_idkurir = "select distinct id_kurir from farmakami.kurir ORDER BY id_kurir ASC"
    cursor.execute(sql_idkurir)
    idkurir = namedtuplefetchall(cursor)
    sql_biaya = "select total_biaya from farmakami.pengantaran_farmasi WHERE id_pengantaran = '" + id +"';"
    cursor.execute(sql_biaya)
    biaya_sql = namedtuplefetchall(cursor)
    biaya = biaya_sql[0].total_biaya
    args = {'form': form, 'idtransaksi': idtransaksi, 'idkurir': idkurir, "id":id, "biaya":biaya, 'message':message}
    return render(request, "updatepf.html", args)


def deletePF(request, id):
    cursor = connection.cursor()
    print(id)
    sql_delete_PF= "delete from farmakami.PENGANTARAN_FARMASI where id_pengantaran = '" +  id + "'"
    cursor.execute(sql_delete_PF)
    return redirect('/pengantaranfarmasi/listPF')
