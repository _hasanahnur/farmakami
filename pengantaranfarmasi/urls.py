from django.urls import path, include
from .views import createPF, listPF, updatePF, deletePF

app_name = "pengantaranfarmasi"
urlpatterns = [
    path('createPF', createPF, name="createPF"),
    path('listPF', listPF, name="listPF"),
    path('updatePF/<id>/', updatePF, name="updatePF"),
    path('deletePF/<id>/', deletePF, name="deletePF"),
]