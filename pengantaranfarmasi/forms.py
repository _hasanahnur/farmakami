from django import forms
import datetime
# from .widgets import BootstrapDateTimePickerInput

class PFForm(forms.Form):
    id_transaksi_pembelian = forms.CharField(label='ID Transaksi', max_length=10)
    waktu = forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M'])
    biaya_kirim = forms.IntegerField(label='Biaya Kirim')


class updatePFForm(forms.Form):
    id_kurir =  forms.CharField(label='ID Kurir', max_length=10, required=False)
    waktu = forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M'], required=False)
    status_pengantaran= forms.CharField(label='Status Pengantaran', max_length=20, required=False)
    biaya_kirim = forms.IntegerField(label='Biaya Kirim', required=False)