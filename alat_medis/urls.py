from django.urls import path, include
from .views import createam, listam, updateam, deleteam

app_name = "am"
urlpatterns = [
    path('createam', createam, name="createam"),
    path('listam', listam, name="listam"),
    path('deleteam/<id>/', deleteam, name="deleteam"),
    path('updateam/<id>/', updateam, name="updateam")
]