from django import forms
class form_am(forms.Form):
    nama_alat_medis = forms.CharField(label='Nama Alat Medis', max_length=50)
    deskripsi = forms.CharField(label='Deskripsi', max_length=100)
    jenis_penggunaan = forms.CharField(label='Jenis Penggunaan', max_length=20)

class form_am_edit(forms.Form):
    nama_alat_medis = forms.CharField(label='Nama Alat Medis', max_length=50, required=False)
    deskripsi = forms.CharField(label='Deskripsi', max_length=100, required=False)
    jenis_penggunaan = forms.CharField(label='Jenis Penggunaan', max_length=20, required=False)