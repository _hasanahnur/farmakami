from django.apps import AppConfig


class AlatMedisConfig(AppConfig):
    name = 'alat_medis'
