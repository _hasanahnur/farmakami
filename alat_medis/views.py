from django.db import connection
from collections import namedtuple

from django.shortcuts import render
from django.shortcuts import render, redirect
from .forms import form_am, form_am_edit

# Create your views here.
def listam(request):
    cursor = connection.cursor()
    cursor.execute("select * from farmakami.alat_medis;")
    table = namedtuplefetchall(cursor)
    return render(request, "listam.html", {'table':table})

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def deleteam(request, id):
    cursor = connection.cursor()
    cursor.execute("delete from farmakami.produk where id_produk ='" + id + "';")
    return redirect("/am/listam")

def createam(request):
    cursor = connection.cursor()
    if request.method == "POST":
        nama_alat_medis = request.POST.get("nama_alat_medis")
        deskripsi = request.POST.get("deskripsi")
        jenis_penggunaan = request.POST.get("jenis_penggunaan")

        cursor.execute("select count(*) from farmakami.produk;")
        banyak_row = cursor.fetchone()[0]
        banyak_row = int(banyak_row)
        fixrow = banyak_row + 1
        fixid = "P" + str(fixrow)
        cursor.execute("select count(*) from farmakami.produk where id_produk ='" + fixid + "';")
        valid = cursor.fetchone()[0]

        while valid != 0:
            fixrow = fixrow + 1
            fixid = "P" + str(fixrow)
            cursor.execute("select count(*) from farmakami.produk where id_produk ='" + fixid + "';")
            valid = cursor.fetchone()[0]
        id_produk = fixid
        #------------------------------------------------------------
        cursor.execute("select count(*) from farmakami.alat_medis;")
        banyak_row = cursor.fetchone()[0]
        banyak_row = int(banyak_row)
        fixrow = banyak_row + 1
        fixid = "AM" + str(fixrow)
        cursor.execute("select count(*) from farmakami.alat_medis where id_alat_medis ='" + fixid + "';")
        count = cursor.fetchone()[0]
        while count != 0:
            fixrow = fixrow + 1
            fixid = "AM" + str(fixrow)
            cursor.execute("select count(*) from farmakami.alat_medis where id_alat_medis ='" + fixid + "';")
            count = cursor.fetchone()[0]
        id_alat_medis = fixid
        cursor.execute("insert into farmakami.produk values('" + id_produk + "');")
        cursor.execute("insert into farmakami.alat_medis values(%s, %s, %s, %s, %s)", [id_alat_medis, nama_alat_medis, deskripsi, jenis_penggunaan, id_produk])
        return redirect("/am/listam")
    

    form = form_am
    return render(request, "createam.html", {'form' : form})

def updateam(request, id):
    cursor = connection.cursor()
    if request.method == "POST":
        nama_alat_medis = request.POST.get("nama_alat_medis")
        deskripsi = request.POST.get("deskripsi")
        jenis_penggunaan = request.POST.get("jenis_penggunaan")

        if nama_alat_medis != "":
            insert = "update farmakami.alat_medis set nama_alat_medis ='" + nama_alat_medis + "' where id_produk='" + id + "'"
            cursor.execute(insert)
        
        if deskripsi != "":
            insert = "update farmakami.alat_medis set deskripsi_alat ='" + deskripsi + "' where id_produk='" + id + "'"
            cursor.execute(insert)
        
        if jenis_penggunaan != "":
            insert = "update farmakami.alat_medis set jenis_penggunaan ='" + jenis_penggunaan + "' where id_produk='" + id + "'"
            cursor.execute(insert)

        return redirect("/am/listam")
    cursor.execute("select id_alat_medis from farmakami.alat_medis where id_produk ='" + id + "';")
    id_alat_medis = cursor.fetchone()[0]
    form = form_am_edit
    return render(request, "updateam.html", {'form' : form, "id_alat_medis" : id_alat_medis, "id_produk": id})




