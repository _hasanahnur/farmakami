from django import forms

class BPForm(forms.Form):
    alamat_balai = forms.CharField(label='Alamat Balai', max_length=100)
    nama_balai = forms.CharField(label='Nama Balai', max_length=50)
    jenis_balai = forms.CharField(label='Jenis Balai', max_length=30)
    telepon_balai = forms.CharField(label='Telepon Balai', max_length=20)

class BPFormEdit(forms.Form):
    alamat_balai = forms.CharField(label='Alamat Balai', max_length=100, required=False)
    nama_balai = forms.CharField(label='Nama Balai', max_length=50, required=False)
    jenis_balai = forms.CharField(label='Jenis Balai', max_length=3, required=False)
    telepon_balai = forms.CharField(label='Telepon Balai', max_length=20, required=False)
    