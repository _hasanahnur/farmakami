from django.shortcuts import render, reverse, redirect
from django.contrib.auth.models import User, Permission
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate, logout, login
from django.conf.urls.static import static
from .forms import BPForm, BPFormEdit
import pandas as pd
from django_tables2.tables import Table
from django.contrib.staticfiles import finders
from django.contrib.staticfiles.storage import staticfiles_storage
from farmakami.settings import STATIC_DIR
from django.db import connection
from collections import namedtuple
import os

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def createbp(request):
    try:
        role = request.session['role']
    except:
        return redirect("/")
    
    if role != 'admin_apotek':
        return redirect("/")

    if request.method =='POST':
        alamat_balai = request.POST.get("alamat_balai")
        nama_balai = request.POST.get("nama_balai")
        jenis_balai = request.POST.get("jenis_balai")
        telepon_balai = request.POST.get("telepon_balai")

        cursor = connection.cursor()

        sumid = "select count(*) from farmakami.balai_pengobatan"
        cursor.execute(sumid)
        sumidfix = cursor.fetchone()[0]
        sql_last_id = "select id_balai from farmakami.balai_pengobatan limit " + str(sumidfix) + " offset " + str((sumidfix-1)) + ""
        cursor.execute(sql_last_id)
        last_id = cursor.fetchone()
        last_id = int(last_id[0][2:])
        last_id = last_id + 1
        fix_id = "BP" + str(last_id)

        #Mencari ID
        sumid = "select count(*) from farmakami.balai_pengobatan"
        cursor.execute(sumid)
        sumidfix = cursor.fetchone()[0]

        test = "BP" + str(sumidfix)
        sql_test = "select count(*) from farmakami.balai_pengobatan where id_balai ='" + test + "'"
        cursor.execute(sql_test)
        hasil = cursor.fetchone()[0]

        while(hasil != 0):
            sumidfix = sumidfix + 1
            test = "BP" + str(sumidfix)
            sql_test = "select count(*) from farmakami.balai_pengobatan where id_balai ='" + test + "'"
            cursor.execute(sql_test)
            hasil = cursor.fetchone()[0]

        fix_id = "BP" + str(sumidfix)
        #Sampe sini
        
        sql_bp = "insert into farmakami.balai_pengobatan VALUES (%s, %s, %s, %s, %s);"
        cursor.execute(sql_bp,(fix_id, alamat_balai, nama_balai, jenis_balai, telepon_balai))

        return redirect('/bp/listbp')
    form = BPForm()
    args = {'form': form}
    cursor.close()
    return render(request, 'createbp.html', args)

def listbp(request):
    dataset = []
    cursor = connection.cursor()
    sql_all_bp= "select * from farmakami.read_bp"
    cursor.execute(sql_all_bp)
    dataset = namedtuplefetchall(cursor)
    table = []
    for t in dataset:
        temp = t.asosiasi.split(",")
        table.append({'row': t, 'asosiasi':temp})
    context = {'table': table}
    cursor.close()
    return render(request, "listbp.html", context)

def deletebp(request, id):
    try:
        role = request.session['role']
    except:
        return redirect("/")
    
    if role != 'admin_apotek':
        return redirect("/")

    cursor = connection.cursor()
    sql_delete_bp= "delete from farmakami.balai_pengobatan where id_balai = '" +  id + "'"
    cursor.execute(sql_delete_bp)
    cursor.close()
    return redirect('/bp/listbp')

def updatebp(request, id):
    try:
        role = request.session['role']
    except:
        return redirect("/")
    
    if role != 'admin_apotek':
        return redirect("/")

    if request.method == "POST":
        alamat_balai = request.POST.get("alamat_balai")
        nama_balai = request.POST.get("nama_balai")
        jenis_balai = request.POST.get("jenis_balai")
        telepon_balai = request.POST.get("telepon_balai")
        apotek = request.POST["id_apotek_select"]
        print("haha")
        print(alamat_balai)

        cursor = connection.cursor()

        if alamat_balai != "":
            sql = "update farmakami.balai_pengobatan set alamat_balai ='" + alamat_balai +"' where id_balai ='" + id + "';"
            cursor.execute(sql)

        if nama_balai != "":
            sql = "update farmakami.balai_pengobatan set nama_balai ='" + nama_balai +"' where id_balai ='" + id + "';"
            cursor.execute(sql)
        
        if jenis_balai != "":
            sql = "update farmakami.balai_pengobatan set jenis_balai ='" + jenis_balai +"' where id_balai ='" + id + "';"
            cursor.execute(sql)
        
        if jenis_balai != "":
            sql = "update farmakami.balai_pengobatan set jenis_balai ='" + jenis_balai +"' where id_balai ='" + id + "';"
            cursor.execute(sql)
        
        if telepon_balai != "":
            sql = "update farmakami.balai_pengobatan set telepon_balai ='" + telepon_balai +"' where id_balai ='" + id + "';"
            cursor.execute(sql)
        
        if apotek != "none":
            check = "select count(*) from farmakami.balai_apotek where id_balai ='" + id + "' and id_apotek = '" + apotek +"'"
            cursor.execute(check)
            checknum = cursor.fetchone()[0]
            if checknum == 0:
                sql = "insert into farmakami.balai_apotek values(%s,%s)"
                cursor.execute(sql, (id, apotek))
        
        return redirect('/bp/listbp')
    print(id)
    cursor = connection.cursor()
    sql_apotek = "select id_apotek from farmakami.apotek"
    cursor.execute(sql_apotek)
    apotek = namedtuplefetchall(cursor)
    form = BPFormEdit()
    args = {'form': form, "apotek":apotek, "id":id }
    cursor.close()
    return render(request, "updatebp.html", args)