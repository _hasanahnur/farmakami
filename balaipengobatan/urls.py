from django.urls import path, include
from .views import createbp, listbp, deletebp, updatebp

app_name = "bp"
urlpatterns = [
    path('createbp', createbp, name="createbp"),
    path('listbp', listbp, name="listbp"),
    path('deletebp/<id>/', deletebp, name="deletebp"),
    path('updatebp/<id>/', updatebp, name="updatebp")
]