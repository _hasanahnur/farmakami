"""farmakami URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from login.views import welcome

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', welcome, name="homepage"),
    path('login/', include(('login.urls', 'login'), namespace='login')),
    path('bp/', include(('balaipengobatan.urls', 'bp'), namespace='bp')),
    path('obat/', include(('obat.urls', 'obat'), namespace='obat')),
    path('am/', include(('alat_medis.urls', 'am'), namespace='am')),
    path('transaksipembelian/', include(('transaksipembelian.urls', 'tp'), namespace='transaksipembelian')),
    path('list_produk_dibeli/', include(('list_produk_dibeli.urls', 'list_produk_dibeli'), namespace='list_produk_dibeli')),
    path('pengantaranfarmasi/', include(('pengantaranfarmasi.urls', 'pengantaranfarmasi'), namespace='pengantaranfarmasi')),
    path('Apotek/', include(('Apotek.urls', 'Apotek'), namespace='Apotek')),
    path('produkapotek/', include(('produkapotek.urls', 'produkapotek'), namespace='produkapotek')),
    path('transaksi_resep/', include(('transaksi_resep.urls', 'transaksi_resep'), namespace='transaksi_resep'))
]

