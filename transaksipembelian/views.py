from django.shortcuts import render, reverse, redirect
from django.contrib.auth.models import User, Permission
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate, logout, login

from django.conf.urls.static import static

from .forms import TPForm, updateTPForm
import pandas as pd
from django_tables2.tables import Table
from django.contrib.staticfiles import finders
from django.contrib.staticfiles.storage import staticfiles_storage

from farmakami.settings import STATIC_DIR
import os
from django.db import connection
from collections import namedtuple

import datetime

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def createTP(request):
    message = ''
    email = request.session['email']
    role = request.session['role']
    cursor = connection.cursor()

    if request.method =='POST':
        try:
            id_konsumen = request.POST["id_konsumen_select"]
            waktu_pembelian = datetime.datetime.now()        

            #Mencari dan membuat ID Transaksi yang baru
            sumid = "select count(*) from farmakami.TRANSAKSI_PEMBELIAN"
            cursor.execute(sumid)
            sumidfix = cursor.fetchone()[0]
            new_id_tp = "TP" + str (sumidfix + 1)

            sql_tp = "insert into farmakami.TRANSAKSI_PEMBELIAN VALUES (%s, %s, %s, %s);"
            cursor.execute(sql_tp, (new_id_tp, waktu_pembelian, 0, id_konsumen))
            print(new_id_tp)
            print(waktu_pembelian)
            return redirect('/transaksipembelian/listTP')
        except Exception as e:
            message = 'Please choose an ID'
        
    if role == "konsumen":
        sql_user = "select id_konsumen from farmakami.KONSUMEN where email ='" + email + "'"
    else:
        sql_user = "select distinct id_konsumen from farmakami.KONSUMEN ORDER BY id_konsumen ASC"
    cursor.execute(sql_user)
    iduser = namedtuplefetchall(cursor)
    form = TPForm()
    args = {'form': form, "id_konsumen":iduser, 'message' : message}
    return render(request, 'createTP.html', args)

def listTP(request):
    email = request.session['email']
    role = request.session['role']
    dataset = []
    cursor = connection.cursor()
    if role == "konsumen":
        sql = "select * from farmakami.konsumen where email ='" + email + "'"
        cursor.execute(sql)
        dataset = namedtuplefetchall(cursor)
        id_konsumen = dataset[0].id_konsumen
        print(id_konsumen)
        sql_all_TP= "select * from farmakami.TRANSAKSI_PEMBELIAN where id_konsumen ='" + id_konsumen + "'"
        cursor.execute(sql_all_TP)
        dataset = namedtuplefetchall(cursor)
        context = {'table': dataset}
        return render(request, "listTP_konsumen.html", context)
    else:
        sql_all_TP= "select * from farmakami.TRANSAKSI_PEMBELIAN"
        cursor.execute(sql_all_TP)
        dataset = namedtuplefetchall(cursor)
        context = {'table': dataset}
        return render(request, "listTP.html", context)

def updateTP(request, id):
    message = ''
    if request.method == 'POST':
        try:
            id_transaksi = request.POST.get("id_transaksi")
            now = datetime.datetime.now()
            waktu_pembelian = str(now)
            id_konsumen = request.POST["id_konsumen_select"]
            print(waktu_pembelian)

            cursor = connection.cursor()

            if id_konsumen != id:
                sql_id = "update farmakami.TRANSAKSI_PEMBELIAN set id_konsumen = '" + id_konsumen + "' where id_transaksi_pembelian ='" + id +"';"
                cursor.execute(sql_id)
                sql_time = "update farmakami.TRANSAKSI_PEMBELIAN set waktu_pembelian = '" + waktu_pembelian + "' where id_transaksi_pembelian ='" + id +"';"
                cursor.execute(sql_time)
            return redirect('/transaksipembelian/listTP')
        except Exception as e:
            message = 'Please choose an ID'
        

    cursor = connection.cursor()
    sql_user = "select distinct id_konsumen from farmakami.KONSUMEN ORDER BY id_konsumen ASC"
    cursor.execute(sql_user)
    iduser = namedtuplefetchall(cursor)

    sql_time = "select waktu_pembelian from farmakami.TRANSAKSI_PEMBELIAN where id_transaksi_pembelian = '" + id +"';"
    cursor.execute(sql_time)
    time = namedtuplefetchall(cursor)
    waktu = time[0].waktu_pembelian

    sql_pembayaran = "select total_pembayaran from farmakami.TRANSAKSI_PEMBELIAN where id_transaksi_pembelian = '" + id +"';"
    cursor.execute(sql_pembayaran)
    total_bayar = namedtuplefetchall(cursor)
    bayar = total_bayar[0].total_pembayaran

    form = updateTPForm()
    args = {'form': form, "id":id, 'message':message, "waktu_pembelian":waktu, "total_pembayaran":bayar, "id_konsumen":iduser}
    return render(request, 'updateTP.html', args)

def deleteTP(request, id):
    cursor = connection.cursor()
    sql_delete_TP= "delete from farmakami.TRANSAKSI_PEMBELIAN where id_transaksi_pembelian = '" +  id + "'"
    cursor.execute(sql_delete_TP)
    return redirect('/transaksipembelian/listTP')
