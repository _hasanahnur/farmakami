from django import forms
import datetime

idkonsumen = [
        ('P1', "P1"),
        ('P2', "P2"),
        ('P3', "P3"),
        ('P4', "P4"),
        ('P5', "P5"),
        ('P6', "P6"),
        ('P7', "P7"),
        ('P8', "P8"),
        ('P9', "P9"),
        ('P10', "P10"),
]

class TPForm(forms.Form):
    waktu_pembelian = forms.DateTimeField(label='Waktu', input_formats=['%d/%m/%Y %H:%M'], widget=forms.HiddenInput())


class OtherTPForm(forms.Form):
    id_transaksi = forms.CharField(label='ID Transaksi', max_length=10)
    waktu_pembelian = forms.CharField(label='Waktu Pembelian', max_length=150)
    total_pembayaran = forms.CharField(label='Total Pembayaran', max_length=10)
    id_konsumen = forms.CharField(label='ID Konsumen', max_length=10)
    
class updateTPForm(forms.Form):
    id_konsumen = forms.ChoiceField(label='ID Konsumen', choices=idkonsumen)