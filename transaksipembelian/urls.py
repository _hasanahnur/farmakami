from django.urls import path, include
from .views import createTP, listTP, updateTP, deleteTP

app_name = "tp"
urlpatterns = [
    path('createTP', createTP, name="createTP"),
    path('listTP', listTP, name="listTP"),
    path('updateTP/<id>/', updateTP, name="updateTP"),
    path('deleteTP/<id>/', deleteTP, name="deleteTP"),
]